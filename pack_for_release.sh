#!/bin/bash
set -o nounset
set -o pipefail
pushd `dirname $0` > /dev/nul;
SCRIPTPATH=`pwd`
popd > /dev/null
pushd $HOME > /dev/null
mkdir -p $SCRIPTPATH/release
echo "Packing $1"
tar Jcf $SCRIPTPATH/release/$1.tar.xz x-tools/$1
popd
